import java.io.*;
import java.util.*;

public class Node{
    public double x; // longtitude
    public double y; // langtitude
    public double f;
    public double g;
    public double h;
    public ArrayList<Node> parents;
    // public int id;
    public Set<Edge> edges; // list of edges

    public Node(double x, double y){
        this.x = x;
        this.y = y;
        // this.id = id;
        this.edges = new HashSet<Edge>();
        this.parents = new ArrayList<Node>(); 
    }

    public Edge add_edge(Node destination, double weight){
        Edge edge = new Edge(this, destination, weight);
        edges.add(edge);
        return edge;
    }

    // removes a node to a specific neighbor
    public double remove_edge(Node neighbor){
        for (Edge edge: edges){
            if (edge.destination==neighbor){
                edges.remove(edge);
                return edge.weight;
            }
        }
        return 0;
    }

    public Queue<Node> get_neighbors(){
        Queue<Node> neighbors = new LinkedList<Node>();
        for (Edge edge : edges){
            neighbors.add(edge.destination);
        }
        return neighbors;
    }

    public int count_neighbors(){
        return edges.size();
    }
}
