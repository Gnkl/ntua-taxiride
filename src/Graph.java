import java.io.*;
import java.util.*;

public class Graph{
    public Set<Node> nodes;
    public Map<Double, Map<Double, Node>> entries;
    // public Map<Double, Map<Double, Node>> nodes;

    public Graph(){
        this.nodes = new HashSet<Node>();
        this.entries = new HashMap<Double, Map<Double, Node>>();
        // this.nodes = new HashMap<Double, Map<Double, Node>>();
    }

    public Node closest_node(double x, double y){
    	Node start = new Node(x, y);
    	Node closest = nodes.iterator().next();
    	double min = get_distance(start, closest);
    	for(Node n : nodes) {
    		double dist = get_distance(start, n);
    		if(dist < min) {
    			min = dist;
    			closest = n;
    		}
    	}
    	return closest;
    }

    //heuristic for each node
    public void heuristic(Node goal) {
    	for(Node node : nodes) {
            node.h = get_distance(node, goal);
        }  
    }

    // rounding function (to 1.0 or 1.25 or 1.5 or 1.75 or 2.0)
    public static double rounding(double num){
        double floor = Math.floor(num);
        double dif = num - floor;
        double res = 0;
        if(dif < 0.125){
            res = (double) floor;
        }else if (dif>0.875){
            res = ((double) floor)+1;
        }else if (dif<0.375){
            res = ((double) floor)+0.25;
        }else if (dif>0.625){
            res = ((double) floor)+0.75;
        }else{
            res = ((double) floor)+0.5;
        }
        if (res==0){
            res = 0.25;
        }
        return res;
    }


    //distance in meters
    public double get_distance(Node source, Node destination) {
        double lat1 = source.x;
        double lon1 = source.y;
        double lat2 = destination.x;
        double lon2 = destination.y;
        int r = 6371; // Radius of the earth in km
        double dLat = Math.toRadians(lat2-lat1);
        double dLon = Math.toRadians(lon2-lon1); 
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon/2) * Math.sin(dLon/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
        double d = (r * c)*1000; // we got d in meters
        // double res = 10 * Math.floor((d + 5) / 10); // round to the nearest tenth
        // if (res == 0){
        //     res = 10;
        // }
        // return res;
        return rounding(d);
    }

    // add node in x, y if it does not already exist in map
    public Node add_node(double x, double y){
        if (!contains(x,y)){
            Node node = new Node(x, y);
            nodes.add(node);
            add_to_map(x,y,node);
            return node;
        }else{
            return this.entries.get(x).get(y);
        }
    }

    public void add_edge(Node n1, Node n2){
        // if (nodes.contains(n1) && nodes.contains(n2))
        double weight = get_distance(n1, n2);
        n1.add_edge(n2, weight);
        n2.add_edge(n1, weight);
        //System.out.println(weight);
    }

    public boolean contains(double x, double y){
        if (!this.entries.containsKey(x)){
            return false;
        }
        if (!this.entries.get(x).containsKey(y)){
            return false;
        }
        return true;
    }

    public void add_to_map(double x, double y, Node node){
        if (! this.entries.containsKey(x)){ // does not contain existing x
            HashMap<Double, Node> minimap = new HashMap<Double, Node>();
            minimap.put(y, node);
            this.entries.put(x, minimap);
        }else{ // contains existing x
            this.entries.get(x).put(y,node); // get the already made minimap for x and put key y and value true
        }
    }



    // deletes a node that has exactly two neighbors
    public void delete_node(Node node){
        double sum = 0;
        Queue<Node> q = node.get_neighbors();
        Node first = q.remove(); // one neighbor
        Node last = q.remove(); // second neighbor
        sum = sum +  first.remove_edge(node);
        sum = sum + last.remove_edge(node);

        //this.add_edge(first, last, sum);
        nodes.remove(node);
        node = null; // for garbage collector
    }

    public void shrink(){
    }
}
