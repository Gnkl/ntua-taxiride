import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.*;
import java.util.*;
import java.lang.*;

public class Main {
    public static WriteFile outfile;
    public static String buffer;
    public static double min_cost;
    public static double best_taxi_id;
    public static double best_taxi_x;
    public static double best_taxi_y;
    public static Node best_source;
    public static Node goal;
    public static Node source;
    public static ArrayList<Node> path;


    //read client.csv and return the result in the form of [[x1,y1], [x2,y2],...].
    public static  ArrayList<ArrayList<Double>> clients(String filename) {
        double x;
        double y;
        ArrayList<ArrayList<Double>> clientsList = new  ArrayList<ArrayList<Double>>();
        try {
            //read client.csv
            File file = new File(filename);
            BufferedReader br = new BufferedReader(new FileReader(file));
            String sCurrentLine = br.readLine();
            while ((sCurrentLine = br.readLine()) != null) {
                ArrayList<Double> clientInfo = new ArrayList<Double>();
                String[] splited_client = sCurrentLine.split(","); 
                x = Double.parseDouble(splited_client[0]);
                y = Double.parseDouble(splited_client[1]);
                clientInfo.add(x);
                clientInfo.add(y);
                clientsList.add(clientInfo);
            }
            return clientsList;
        }
        catch(IOException ioe) {
            System.out.println("Trouble reading from file : " + ioe.getMessage());
            return null;
        }
    }



    //read taxis.csv and return the result in the form of [[x1,y1], [x2,y2],...].
    public static  ArrayList<ArrayList<Double>> taxis(String filename) {
        double x;
        double y;
        double id;
        ArrayList<ArrayList<Double>> taxisList = new  ArrayList<ArrayList<Double>>();
        try {
            //read taxis.csv
            File file = new File(filename);
            BufferedReader br = new BufferedReader(new FileReader(file));
            String sCurrentLine = br.readLine();
            while ((sCurrentLine = br.readLine()) != null) {
                ArrayList<Double> taxiInfo = new ArrayList<Double>();
                String[] splited_taxi = sCurrentLine.split(","); 
                x = Double.parseDouble(splited_taxi[0]);
                y = Double.parseDouble(splited_taxi[1]);
                id = Double.parseDouble(splited_taxi[2]);
                taxiInfo.add(x);
                taxiInfo.add(y);
                taxiInfo.add(id);
                taxisList.add(taxiInfo);
            }
            return taxisList;
        }
        catch(IOException ioe) {
            System.out.println("Trouble reading from file : " + ioe.getMessage());
            return null;
        }
    }



    public static Graph createGraph(String filename) {
        double x;
        double y;
        int id;
        int prev_id = 0;

        try {
            Graph g = new Graph();
            Node node;
            Node prev_node = null;

            //read nodes.csv
            File file = new File(filename);
            BufferedReader br = new BufferedReader(new FileReader(file));
            String sCurrentLine = br.readLine(); 
            while ((sCurrentLine = br.readLine()) != null) { 
                String[] splited_nodes = sCurrentLine.split(","); 
                x = Double.parseDouble(splited_nodes[0]);
                y = Double.parseDouble(splited_nodes[1]);
                id = Integer.parseInt(splited_nodes[2]);
                
                node = g.add_node(x,y);
                if (prev_id == id) {
                    g.add_edge(node, prev_node);
                }
                prev_id = id;
                prev_node = node;
            }
            return g;

        }
        catch(IOException ioe) {
            System.out.println("Trouble reading from file : " + ioe.getMessage());
            return null;
        }
    }



    public static void AstarSearch(Node source, Node goal){
        Set<Node> closed_set = new HashSet<Node>();
        PriorityQueue<Node> q = new PriorityQueue<Node>(10, 
            new Comparator<Node>(){
                //override compare method
                public int compare(Node node1, Node node2){
                    if(node1.f > node2.f){
                        return 1;
                    }
                    else if (node1.f < node2.f){
                        return -1;
                    }
                    else{
                        return 0;
                    }
                }
            }
        );

        //cost from start
        source.g = 0;
        q.add(source);
        boolean found = false;

        while((!q.isEmpty())&&(!found)){
            //the node in having the lowest f_score value
            Node current = q.poll();
            closed_set.add(current);

            //goal found
            if((current.x == goal.x) && (current.y == goal.y)){
                found = true;
                // break;
            }

            //check every child of current node
            for(Edge e : current.edges){
                Node child = e.destination;
                double weight = e.weight;
                double temp_g = current.g + weight;
                double temp_f = temp_g + child.h;

                /*if child node has been evaluated and 
                the newer f_score is higher, skip*/
                if((closed_set.contains(child)) && (temp_f > child.f)){
                    continue;
                }

                if((q.contains(child)) && (temp_f > child.f)){
                    continue;
                }

                /*else if child node is not in queue or 
                newer f_score is lower*/ 
                if (!q.contains(child) || temp_f <= child.f){
                    if (temp_f < child.f){
                        child.parents.clear();
                    }
                    child.parents.add(current);
                    child.g = temp_g;
                    child.f = temp_f;

                    // remove and add again to find the right place
                    if(q.contains(child)){
                        q.remove(child);
                    }
                    q.add(child);
                }
                
            }
        }
    }

    // recursive function
    // child is the client this time and target is the taxi (father)
    public static void printPath(Node child, Node goal, ArrayList<Node> Acc){
        if (child.x == goal.x && child.y == goal.y){
            outfile.addString("New Route: ");
            for (Node i : Acc){
                outfile.addString(i.x + "," + i.y);
            }
            outfile.addString(" ");
        } else {
            for(Node parent : child.parents){
                Acc.add(parent);
                printPath(parent, goal, Acc);
                Acc.remove(parent);
            }
        }
    }

    // recursive function
    public static void printScreenPath(Node child, Node goal, ArrayList<Node> Acc){
        if (child.x == goal.x && child.y == goal.y){
            System.out.println("New Route: ");
            for (Node i : Acc){
                System.out.println(i.x + "," + i.y);
            }
            System.out.println(" ");
        } else {
            for(Node parent : child.parents){
                Acc.add(parent);
                printScreenPath(parent, goal, Acc);
                Acc.remove(parent);
            }
        }
    }

    public static void main(String []args){
        // constants
        final String nodesFile = "nodes.csv";
        final String clientsFile = "client.csv";
        final String taxisFile = "taxis.csv";
        final String resultsFile = "results.txt";
        path = new ArrayList<Node>();

        Graph graph = createGraph(nodesFile);
        ArrayList<ArrayList<Double>> clients = clients(clientsFile);
        ArrayList<ArrayList<Double>> taxis = taxis(taxisFile);
        outfile = new WriteFile(resultsFile); // can be referenced from every method
        int client_no = 1;

        while(!clients.isEmpty()) {
            ArrayList<Double> client_xy = clients.remove(0);
            double client_x = client_xy.remove(0);
            double client_y = client_xy.remove(0);
            goal = graph.closest_node(client_x, client_y);
            outfile.addString("--------");
            outfile.addString("New clients (no " + client_no + ") -> client coordinates:" + client_x +","+ client_y + " and goal node coordinates:" + goal.x +","+ goal.y);
            outfile.addString(" ");
            graph.heuristic(goal);

            // keep the best taxi and cost
            min_cost = 1000000;

            while(!taxis.isEmpty()) {
                ArrayList<Double> taxi_xy = taxis.remove(0);
                double taxi_x = taxi_xy.remove(0);
                double taxi_y = taxi_xy.remove(0);
                double taxi_id = taxi_xy.remove(0);
                
                source = graph.closest_node(taxi_x, taxi_y);
                outfile.addString("------------------------");
                outfile.addString("New taxi -> id:" +taxi_id+ " with taxi coordinates:" + taxi_x +","+ taxi_y + " and starting node coordinates:" + source.x +","+ source.y);
                outfile.addString(" ");
                AstarSearch(source, goal);
                path.add(goal);
                printPath(goal, source, path);
                buffer = String.format("Total cost : %.2f meters", goal.g);
                outfile.addString(buffer);
                // find the best taxi
                if (goal.g < min_cost){
                    min_cost = goal.g;
                    best_taxi_id = taxi_id;
                    best_taxi_x = taxi_x;
                    best_taxi_y = taxi_y;
                    best_source = source;
                }
                // clear the parents of all nodes
                path.clear();
                for (Node n: graph.nodes){
                    n.parents.clear();
                    n.f = 0;
                    n.g = 0;
                }
            }  
            System.out.printf("The best taxi for client no%d has id=%.0f and must travel %.2f meters\n", client_no, best_taxi_id, min_cost);
            client_no += 1;
            AstarSearch(best_source, goal);
            path.add(goal);
            printScreenPath(goal, best_source, path);
            // clear the parents of all nodes
            path.clear();
            for (Node n: graph.nodes){
                n.parents.clear();
                n.f = 0;
                n.g = 0;
            }
        }
        outfile.produce();
        System.out.printf("For more details about the routes check out the "+ resultsFile +" file\n");
    }

}