import java.io.*;
import java.util.*;

public class WriteFile {
    public String filename;
    public String dirPath;
    public List<String> l;

    // for creating a new directory
    // public WriteFile(String dirName){
    //     this.dirPath = "./" + dirName;
    //     this.filename = dirPath + "/results.csv";
    //     this.l = new ArrayList<String>();
    //     new File(dirPath).mkdirs();
    // }
    public WriteFile(String filename){
        this.filename = filename;
        this.l = new ArrayList<String>();
    }


    public void addString(String str){
        this.l.add(str);
    }

    public void produce(){
        try {
            //Whatever the file path is.
            FileOutputStream is = new FileOutputStream(this.filename);
            OutputStreamWriter osw = new OutputStreamWriter(is);    
            Writer w = new BufferedWriter(osw);
            for (String str : this.l){
                w.write(str);
                w.write("\n");
            }
            w.close();
        } catch (IOException e) {
            System.err.println("Problem writing to the file statsTest.txt");
        } 
    }
}