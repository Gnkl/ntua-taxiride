# 🚕 ntua-taxiride (do a taxi ride)

The ntua-taxiride is a program that finds taxi routes. Given the position of a client, the destination he has and the current locations of the taxi drivers, the programs selects the most appropriate driver and finds the most efficient route for him to pick up the client and carry him to his destination.

The program is written in java and uses the a-star algorithm to calculate fastest routes.

The code is saved in the src/ folder.
After running Main.java with some client.csv, taxis.csv and nodes.csv, the results.txt file is produced and contains all the necessary information.
In the senarios/ folder exist 3 complete examples.

---
This project was part of the lab exercises for the course "Artificial Intelligence" of Electrical and Computer Engineering of National Technical University of Athens. 
